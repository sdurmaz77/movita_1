package actions;

import controls.DropdownControl;

public class DropdownActions {



    public static void click(String label) {
        DropdownControl.fromLabel(label).isVisible();
        DropdownControl.fromLabel(label).hoverOver();
        click(label, "button");

    }

    public static void click(String label, String type) {
        switch (type.toLowerCase().trim()) {
            case "locator":
                DropdownControl.fromLocator(label)
                        .click();
                break;
            default:
                DropdownControl.fromLabel(label)
                        .click();

        }
    }

}
